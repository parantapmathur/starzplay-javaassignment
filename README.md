# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository will have the source code of the assigment given.

### Challenges and Assumption ###
- I faced challenge in integrating Google API due to time constrain and Google credential issues. Due this challenge we are not able fetch file from Google Drive.
	- based on the above challenge, I took 2 assumption that :
		a. we will get json response from other "get" restService exposed 
		or 
		b. files will be stored locally on the the system.

### Brief Description jhipster-sample-application ###

- This application is using JHipster framework which has a spring boot framework.
	- Jhipster will provide everyThing for the application from, source structure to build and to deploy.
	- Jhipster is a quick way to start a project and go live.
	- jhipster-sample-application has everything which include the application framework, service access and security with deployment server, for future use of this 	application this has all the stuff for UI and it will handle MVC.  
	- Service Name: StarzResources
	- package name: io.github.jhipster.sample.web.rest

### How do I get set up? ###

1. import project jhipster-sample-application in Eclipse STS as a maven project.
	2. open file hipster-sample-app-master\src\main\resources\config\application-prod.yml
		a. if required add/change comma seperated values for:
			i. fliter values to allow in request param, currently "censoring" is configured in "startsFilterParam" 
			ii. level values to allow in request param, currently "censored,uncensored" is configured in "startsLevelParam" 
		b. configuring the Json to filter :
			i. if we have make rest call to get the json response add the service url in "remoteServiceURL".
			ii. if json file is stored locally then : add the file path location in "filePath" and make "remoteServiceURL" as blank.
	4. right click on project and select "run as" then select "mvn install"
	5. right click on project and select "run as" then select "spring boot app".
	6. application will run on the 8080 port.
	7. Open soap-ui and create a new rest project and the url: http://<server-IP>:8080/media?filter=<configured value in startsFilterParam>&level=<configured value in startsLevelParam>
		e.g.: http://localhost:8080/media?filter=censoring&level=censored
	8. based on the filer and level request query param, content.json file will filtered.


